#include <iostream>
#include <fstream>
#include <unistd.h>

using namespace std;

#define GPIO_ACTIVE_LOW     0
#define GPIO_ACTIVE_HIGH    1

#define LCD_RS      "/sys/class/leds/LCD_RS/brightness"
#define LCD_RW      "/sys/class/leds/LCD_RW/brightness"
#define LCD_EN      "/sys/class/leds/LCD_EN/brightness"
#define LCD_D4      "/sys/class/leds/LCD_D4/brightness"
#define LCD_D5      "/sys/class/leds/LCD_D5/brightness"
#define LCD_D6      "/sys/class/leds/LCD_D6/brightness"
#define LCD_D7      "/sys/class/leds/LCD_D7/brightness"

std::ofstream LCD_RS_brightness;
std::ofstream LCD_RW_brightness;
std::ofstream LCD_EN_brightness;
std::ofstream LCD_D4_brightness;
std::ofstream LCD_D5_brightness;
std::ofstream LCD_D6_brightness;
std::ofstream LCD_D7_brightness;

int GPIO_LCD_Config(void);
void LCD_Enable(void);
void LCD_Send4Bit(unsigned char Data);
void LCD_SendCommand(unsigned char command);
void LCD_Clear(void);
void LCD_Init(void);
void LCD_Gotoxy(unsigned char x, unsigned char y);
void LCD_PutChar(unsigned char Data);
void LCD_Puts(char *s);
void TempShow(unsigned char z);
void Close_File(void);

int main()
{
    while(1)
    {
        LCD_Init();	

        LCD_Gotoxy(3,0);
        LCD_Puts("PTIT-KTDT1");
        LCD_Gotoxy(4,1);
        LCD_Puts("Lab AIoT");

        usleep(500000);
    }

    Close_File();
    return 0;
}

int GPIO_LCD_Config(void)
{
    LCD_RS_brightness.open(LCD_RS, std::ios::trunc);
    LCD_RW_brightness.open(LCD_RW, std::ios::trunc);
    LCD_EN_brightness.open(LCD_EN, std::ios::trunc);
    LCD_D4_brightness.open(LCD_D4, std::ios::trunc);
    LCD_D5_brightness.open(LCD_D5, std::ios::trunc);
    LCD_D6_brightness.open(LCD_D6, std::ios::trunc);
    LCD_D7_brightness.open(LCD_D7, std::ios::trunc);

    if (!LCD_RS_brightness.is_open() || !LCD_RW_brightness.is_open() || !LCD_EN_brightness.is_open()
        || !LCD_D4_brightness.is_open() || !LCD_D5_brightness.is_open() || !LCD_D6_brightness.is_open() 
        || !LCD_D7_brightness.is_open()
    ) 
    {
        cerr << "Failed to open the brightness file." << endl;
        return 0;
    }
    return 1;
}

void LCD_Enable(void)
{
    LCD_EN_brightness << GPIO_ACTIVE_HIGH;
    LCD_EN_brightness.flush();
	usleep(3);
	LCD_EN_brightness << GPIO_ACTIVE_LOW;
    LCD_EN_brightness.flush();
	usleep(50); 
}

void LCD_Send4Bit(unsigned char Data)
{
    LCD_D4_brightness << (Data & 0x01);
    LCD_D4_brightness.flush();
    LCD_D5_brightness << ((Data >> 1) & 0x01);
    LCD_D5_brightness.flush();
    LCD_D6_brightness << ((Data >> 2) & 0x01);
    LCD_D6_brightness.flush();
    LCD_D7_brightness << ((Data >> 3) & 0x01);
    LCD_D7_brightness.flush();
}

void LCD_SendCommand(unsigned char command)
{
	LCD_Send4Bit(command >> 4);
	LCD_Enable();
	LCD_Send4Bit(command);
	LCD_Enable();
}

void LCD_Clear(void)
{
	LCD_SendCommand(0x01);
	usleep(10);
}

void LCD_Init(void)
{
	GPIO_LCD_Config();
	LCD_Send4Bit(0x00);
    LCD_RS_brightness << GPIO_ACTIVE_LOW;
    LCD_RS_brightness.flush();
    LCD_RW_brightness << GPIO_ACTIVE_LOW;
    LCD_RW_brightness.flush();
	LCD_Send4Bit(0x03);
	LCD_Enable();
	LCD_Enable();
	LCD_Enable();
	LCD_Send4Bit(0x02);
	LCD_Enable();
	LCD_SendCommand(0x28); // giao thuc 4 bit, hien thi 2 hang, ki tu 5x8
	LCD_SendCommand(0x0c); // cho phep hien thi man hinh
	LCD_SendCommand(0x06); // tang ID, khong dich khung hinh
	LCD_SendCommand(0x01); // xoa toan bo khung hinh
	usleep(10000);
}

void LCD_Gotoxy(unsigned char x, unsigned char y)
{
	unsigned char address;
	if(y == 0)
    {
        address = (0x80 + x);
    }
	else if(y == 1) 
    {
        address=(0xc0+x);
    }

	usleep(1000);
	LCD_SendCommand(address);
	usleep(50);
}

void LCD_PutChar(unsigned char Data)
{
    LCD_RS_brightness << GPIO_ACTIVE_HIGH;
    LCD_RS_brightness.flush();
	LCD_SendCommand(Data);
	LCD_RS_brightness << GPIO_ACTIVE_LOW;
    LCD_RS_brightness.flush();
}

void LCD_Puts(char *s)
{
	while (*s)
	{
		LCD_PutChar(*s);
		s++;
	}
}

void TempShow(unsigned char z)
{
	LCD_PutChar((z/100)+48); //Tram
	LCD_PutChar((z%100/10)+48); //Chuc
	LCD_PutChar((z%10)+48); //Don vi
}

void Close_File(void)
{
    LCD_RS_brightness << GPIO_ACTIVE_LOW;
    LCD_RS_brightness.flush();
    LCD_RW_brightness << GPIO_ACTIVE_LOW;  
    LCD_RW_brightness.flush();
    LCD_EN_brightness << GPIO_ACTIVE_LOW;  
    LCD_EN_brightness.flush();
    LCD_D4_brightness << GPIO_ACTIVE_LOW;  
    LCD_D4_brightness.flush();
    LCD_D5_brightness << GPIO_ACTIVE_LOW;  
    LCD_D5_brightness.flush();
    LCD_D6_brightness << GPIO_ACTIVE_LOW;  
    LCD_D6_brightness.flush();
    LCD_D7_brightness << GPIO_ACTIVE_LOW;    
    LCD_D7_brightness.flush();

    LCD_RS_brightness.close();
    LCD_RW_brightness.close();  
    LCD_EN_brightness.close();  
    LCD_D4_brightness.close();  
    LCD_D5_brightness.close();  
    LCD_D6_brightness.close();  
    LCD_D7_brightness.close();    
}
