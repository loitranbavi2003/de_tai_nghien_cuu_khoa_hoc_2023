#include <mosquitto.h>
#include <mqtt_protocol.h>
#include <jsoncpp/json/json.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <iostream>

#define HOST        "Mqtt.mysignage.vn"
#define PORT        1883
#define PUB_TOPIC   "BLINK_LED"
#define CLIENT_ID   "client_pub"

using namespace std;

int count = 0, flag = 0;
const char* msg_led;

struct mosquitto *mosq;

int main(int argc, char * argv[])
{
    mosquitto_lib_init();
	mosq = mosquitto_new(CLIENT_ID, true, NULL);

    if(mosq)
    {
	    mosquitto_connect(mosq, HOST, PORT, 60);
	}

    while(1)
    {
        Json::Value message;
        message["Device"] = "Led";
        
        if(flag == 1)
        {
            flag = 0;
            message["Value_led"] =  1;
        }
        else
        {
            flag = 1;
            message["Value_led"] =  0;
        }

        string json_string;
        Json::FastWriter writer;
        json_string = writer.write(message);
        msg_led = json_string.c_str();

        cout << "Connection active!" << endl;
        cout << "Number message: " << count++ << endl;
        cout << msg_led << endl;

        mosquitto_publish(mosq, NULL, PUB_TOPIC, strlen(msg_led), msg_led, 0, false);
        sleep(2);

        mosquitto_loop(mosq, -1, 1);
    }
} 
