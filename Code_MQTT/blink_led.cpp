#include <iostream>
#include <fstream>
#include <unistd.h>

using namespace std;

int main()
{
    std::ofstream outputFile("/sys/class/leds/led_green/brightness", std::ios::trunc);
    if (!outputFile.is_open()) 
	{
        cerr << "Failed to open the /sys/class/leds/led_green/brightness file." << endl;
        return 1;
    }

    while(1)
    {
        cout << "valid: 1" << endl;
        outputFile << 1;  
        outputFile.flush();  
        sleep(1);
        
        cout << "valid: 0" << endl;
        outputFile << 0;
        outputFile.flush();  
        sleep(1);
    }

    outputFile.close();  
    return 0;
}
