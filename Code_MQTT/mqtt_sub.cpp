#include <mosquitto.h>
#include <mqtt_protocol.h>
#include <json/json.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <iostream>
#include <fstream>

#define HOST        "Mqtt.mysignage.vn"
#define PORT        1883
#define SUB_TOPIC	"BLINK_LED"
#define CLIENT_ID   "client_sub"

using namespace std;

struct mosquitto *mosq;

string message_topic_str;
string message_payload_str;

std::ofstream outputFile("/sys/class/leds/Led_Green/brightness", std::ios::trunc);

void message_callback(struct mosquitto *mosq, void *obj, const struct mosquitto_message *message)
{
    cout << "Got message from " << message->topic << " topic" << endl;
    cout << "Content: " << (char*) message->payload << endl;
    message_payload_str = (char*) message->payload;
    message_topic_str = message->topic;

    Json::Reader     reader;
    Json::Value      root;
    Json::FastWriter writer;
    
    if (reader.parse(message_payload_str, root)) 
	{
        if (root.isMember("Value_led") && root["Value_led"].isInt()) 
		{
            int value_led = root["Value_led"].asInt();
            cout << "Value_led: " << value_led << endl;

            if(value_led == 1)
			{
				outputFile << 1;  
				outputFile.flush();
			}
			else
			{
				outputFile << 0;  
				outputFile.flush();
			}
        } 
		else 
		{
            cout << "Invalid or missing Value_led field in the JSON message." << endl;
        }
    } 
	else 
	{
        cout << "Failed to parse the JSON message." << endl;
    }
}


int main(int argc, char * argv[]) 
{
	if (!outputFile.is_open()) 
	{
        cerr << "Failed to open the /sys/class/leds/Led_Green/brightness file." << endl;
        return 1;
    }
	
    mosquitto_lib_init();
	mosq = mosquitto_new(CLIENT_ID, true, NULL);

	if(mosq)
    {
	    mosquitto_connect(mosq, HOST, PORT, 60);
        mosquitto_message_callback_set(mosq, message_callback);
		mosquitto_subscribe(mosq, NULL, SUB_TOPIC, 0);	
	}

    while(1)
    {
        mosquitto_loop(mosq, 3000, 1);
    }
} 
